resource "aws_lb" "app_lb" {
  name = "${local.name_prefix}-app-ALB"
  internal = true
  load_balancer_type = "application"
  idle_timeout = 600
  security_groups = ["value"]
}

