resource "aws_vpc" "COURSE_VPC" {
  cidr_block = var.vpc_cidr
  enable_dms_hostnames = true
  tags = merge({
    "Name" = "${local.name_prefix}.VPC",
  },    
    local.default_tags,
  )  
}

resource "aws_internet_gateway" "COURSE_IGW" {
  vpc_id =  aws_vpc.COURSE_VPC.id
  tags = merge({
    "Name" = "${local.name_prefix}.IGW",
  },
    local.default_tags,
  )
}

resource "aws_subnet" "COURSE_PUBLIC_SUBNET" {
  map_public_ip_on_launch = true
  avilability_zone = element(var.az_name, 0)
  vpc_id = aws.COURSE_VPC.id
  cidr_block = element(var.subnet_cidr_blocks, 0)
  tags = merge({
    "Name" = "${local.name_prefix}.SUBNET-AZ-A",
  },
    local.default_tags,
  )
}

resource "aws_subnet" "COURSE_PRIVATE_SUBNET" {
  map_public_ip_on_launch = false
  avilability_zone = element(var.az_name, 1)
  vpc_id = aws.COURSE_VPC.id
  cidr_block = element(var.subnet_cidr_blocks, 1)
  tags = merge({
    "Name" = "${local.name_prefix}.SUBNET-AZ-B",
  },
    local.default_tags,
  )
}

resource "aws_eip" "APP_EIP" {
}

resource "aws_nat_geteway" "COURSE_NAT" {
  subnet_id = aws_subnet.COURSE_PUBLIC_SUBNET.id
  allocation_id = aws_elp.APP_EIP.id
  tags = merge({
    "Name" = "${local.name_prefix}-NGW",
  },
    local.default_tags,
  )
}

resource "aws_route_table" "COURSE_PUBLIC_ROUTE" {
  vpc_id = aws_vpc.COURSE_VPC.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.COURSE_IGW.id
  }
  tags = merge({
    "Name" = "${local.name_prefix}-PUBLIC-RT",
  },
    local.default_tags,
  )
}

resource "aws_route_table" "COURSE_PRIVATE_ROUTE" {
  vpc_id = aws_vpc.COURSE_VPC.id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_geteway.COURSE_NAT.id
  }
  tags = merge({
    "Name" = "${local.name_prefix}-PRIVATE_RT",
  },
    local.default_tags,
  )
}

resource "aws_vpc_endpoint" "COURSE_S3_ENDPOINT" {
  vpc_id = aws_vpc.COURSE_VPC.id
  service_name = "com.amazonaws.${var.aws_region}.s3"
  route_table_ids = [aws_route_table.COURSE_PUBLIC_ROUTE.id, aws_route_table.COURSE_PRIVATE_ROUTE.id]
}

resource "aws_route_table_association" "PUBLIC_ASSO" {
  route_table_id = aws_route_table.COURSE_PUBLIC_ROUTE.id
  subnet_id = aws_subnet.COURSE_PUBLIC_SUBNET.id
}


resource "aws_route_table_association" "PRIVATE_ASSO" {
  route_table_id = aws_route_table.COURSE_PRIVATE_ROUTE.id
  subnet_id = aws_subnet.COURSE_PRIVATE_SUBNET.id
}

resource "aws_network_acl" "COURCE_NACL" {
  vpc_id = aws_vpc.COURSE_VPC.id
  subnet_ids = [aws_route_table.COURSE_PUBLIC_ROUTE.id, aws_route_table.COURSE_PRIVATE_ROUTE.id]
  ingress {
    protocol = "tcp"
    rule_no = 110
    action = "deny"
    cidr_block = "0.0.0.0/0"
    from_port = 23
    to_port = 23
  }  
  ingress {
    protocol = "tcp"
    rule_no = 32706
    action = "allow"
    cidr_block = "0.0.0.0/0"
    from_port = 0
    to_port = 0
  }
  egress {
    protocol = "tcp"
    rule_no = 110
    action = "deny"
    cidr_block = "0.0.0.0/0"
    from_port = 23
    to_port = 23
  }  
  egress {
    protocol = "tcp"
    rule_no = 32706
    action = "allow"
    cidr_block = "0.0.0.0/0"
    from_port = 0
    to_port = 0
  }
  tags = merge({
    "Name" = "${local.name_prefix}-NACL"
  },  
    local.default_tags
  )
}

resource "aws_security_group" "APP_ALB_SG" {
  vpc_id = aws_vpc.COURSE_VPC.subnet_id
  name = "${local.name_prefix}-ALB-SG"
  ingress{
    from_port = 80
    to_port = 80
    protocol = "tcp"
    security_group = [aws_security_group.APP_SG.id]
  }
  ingress{
    from_port = 443
    to_port = 443
    protocol = "tcp"
    security_group = [aws_security_group.APP_SG.id]
  }
  egress{
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge({
    "Name" = "${local.name_prefix}-SG-LB",
  },
    local.default_tags,
  )
}



resource "aws_security_group" "APP_SG" {
  vpc_id = aws_vpc.COURSE_VPC.subnet_id
  name = "${local.name_prefix}-SG-APP"
  ingress{
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.COURSE_VPC.cidr_block]
  }
  ingress{
    from_port   = 3300
    to_port     = 3300
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
  }
  ingress{
    from_port = 80
    to_port = 80
    protocol = "tcp"
    security_group = [aws_security_group.APP_SG.id]
  }  
  egress{
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  lifecycle {
    create_before_destroy = true
  }
  tags = merge({
    "Name" = "${local.name_prefix}-SG-LB",
  },
    local.default_tags,
  )
}